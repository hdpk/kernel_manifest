Google's Android Kernel Build System
=====================================

The Pixel 8 Pro (codenamed _"Husky"_) is a flagship smartphone from Google.
It was released October 04th of 2023.

| Basic                   | Spec Sheet                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
| CPU                     | Nona-core (1x3.0 GHz Cortex-X3 & 4x2.45 GHz Cortex-A715 & 4x2.15 GHz Cortex-A510)                                              |
| Chipset                 | Google Tensor G3 (4 nm)                                                                                                        |
| GPU                     | Immortalis-G715s MC10                                                                                                          |
| Memory                  | 12 GB RAM                                                                                                                      |
| Shipped Android Version | 14                                                                                                                             |
| Storage                 | 128GB/256GB/512GB/1TB                                                                                                          |
| Battery                 | Non-removable Li-Ion 5050 mAh battery                                                                                          |
| Display                 | LTPO OLED, 120Hz, HDR10+, 1600 nits (HBM), 2400 nits (peak)                                                                    |
| Display (Size)          | 6.7 inches, 108.7 cm2 (~87.4% screen-to-body ratio)                                                                            |
| Display (Resolution)    | 1344 x 2992 pixels, 20:9 ratio (~489 ppi density)                                                                              |
| Camera (Back)           | 50 MP, f/1.7, 25mm (wide), 1/1.31", 1.2µm, dual pixel PDAF, multi-zone Laser AF, OIS                                           |
| Camera (Back)           | 48 MP, f/2.8, 113mm (telephoto), 1/2.55", 0.7µm, dual pixel PDAF, OIS, 5x optical zoom                                         |
| Camera (Back)           | 48 MP, f/2.0, 126˚ (ultrawide), 0.8µm, dual pixel PDAF                                                                         |
| Video (Back)            | 4K@30/60fps, 1080p@24/30/60/120/240fps; gyro-EIS, OIS, 10-bit HDR                                                              |
| Camera (Front)          | 10.5 MP, f/2.2, 20mm (ultrawide), 1/3.1", 1.22µm, PDAF                                                                         |
| Video (Front)           | 4K@24/30/60fps, 1080p@30/60fps                                                                                                 |

![Pixel 8 Pro](https://fdn2.gsmarena.com/vv/pics/google/google-pixel-8-pro-1.jpg "Pixel 8 Pro")

Current Branch: android-gs-shusky-6.1-android15-qpr2-beta

Custom Branch: sh15-6.1

Everything is currently pulled from google save for a few repositories listed here:

>"android | kernel manifest"
>https://gitlab.com/hdpk/kernel_manifest/-/tree/sh15-6.1

>"anykernel3"
>https://gitlab.com/hdpk/anykernel3/-/tree/sh15-6.1

>"build/kernel"
>https://gitlab.com/hdpk/android_build/-/tree/sh15-6.1

>"aosp | kernel_common"
>https://gitlab.com/hdpk/kernel_common/-/tree/sh15-6.1

>"private/devices/google/common"
>https://gitlab.com/hdpk/devices_google_common/-/tree/sh15-6.1

>"private/devices/google/gs201"
>https://gitlab.com/hdpk/devices_google_gs201/-/tree/sh15-6.1

>"private/devices/google/pantah"
>https://gitlab.com/hdpk/devices_google_pantah/-/tree/sh15-6.1

>"private/devices/google/shusky"
>https://gitlab.com/hdpk/devices_google_shusky/-/tree/sh15-6.1

>"private/devices/google/zuma"
>https://gitlab.com/hdpk/devices_google_zuma/-/tree/sh15-6.1

>"private/google-modules/gpu"
>https://gitlab.com/hdpk/google-modules_gpu/-/tree/sh15-6.1

>"private/google-modules/soc/gs"
>https://gitlab.com/hdpk/google-modules_soc_gs/-/tree/sh15-6.1

>"private/google-modules/wlan/bcm4398"
>https://gitlab.com/hdpk/google-modules_wlan_bcm4398/-/tree/sh15-6.1

>"private/google-modules/wlan/bcm4383"
>https://gitlab.com/hdpk/google-modules_wlan_bcm4383/-/tree/sh15-6.1


Warning: Occasionally things may be changed or force-pushed without notice.


In Order To Build
====================

>mkdir -p p815 && cd p815

>repo init -u https://gitlab.com/hdpk/kernel_manifest -b sh15-6.1

>repo sync -j 8

>time ./build_shusky.sh

>Flashable zip generated in 'upload' directory
